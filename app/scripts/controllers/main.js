'use strict';

/**
 * @ngdoc function
 * @name eportfolio2WebApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the eportfolio2WebApp
 */
angular.module('eportfolio2WebApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
