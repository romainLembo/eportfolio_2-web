'use strict';

/**
 * @ngdoc function
 * @name eportfolio2WebApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the eportfolio2WebApp
 */
angular.module('eportfolio2WebApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
